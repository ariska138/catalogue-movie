package com.ariskahidayat.mycatalogmovie.View;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.Database.CursorMovie;
import com.ariskahidayat.mycatalogmovie.Database.FavoriteHelper;
import com.ariskahidayat.mycatalogmovie.Database.MovieColumns;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.R;
import com.squareup.picasso.Picasso;

import java.net.URI;

import static com.ariskahidayat.mycatalogmovie.Database.DatabaseContract.CONTENT_URI;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 8:23 AM
 ******************************************************************************/

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imageDropdown, imagePoster, imageFavorite;
    TextView textOverview, textTitle, textRate, textRelease;
    boolean isFavorite = false;

    private Movie movieItem;
    private FavoriteHelper favoriteHelper;

    boolean isMovieReady = false;
    Button btnBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        movieItem = getIntent().getParcelableExtra(Const.INFO_DETAIL);
        isMovieReady = getIntent().getBooleanExtra(Const.MOVIE_READY, false);
        isFavorite = getIntent().getBooleanExtra(Const.FAVORITE_MOVIE, false);

        imageDropdown = (ImageView) findViewById(R.id.detail_img_dropdown);
        imagePoster = (ImageView) findViewById(R.id.detail_img_poster);
        textOverview = (TextView) findViewById(R.id.detail_tv_overview);
        textTitle = (TextView) findViewById(R.id.detail_tv_title);
        textRate = (TextView) findViewById(R.id.detail_tv_rate);
        textRelease = (TextView) findViewById(R.id.detail_tv_release);
        imageFavorite = (ImageView) findViewById(R.id.detail_img_favorite);
        btnBuy = (Button) findViewById(R.id.detail_btn_buy);

        btnBuy.setOnClickListener(this);
        imageFavorite.setOnClickListener(this);

        if (!isMovieReady) {
            btnBuy.setEnabled(false);
            btnBuy.setText("Coming Soon");
        }
        //  getSupportActionBar().setTitle(getIntent().getStringExtra(MainActivity.TITLE_DETAIL));
        textTitle.setText(" : " + movieItem.getTitle());
        textRate.setText(" : " + movieItem.getVote() + "");
        textOverview.setText(movieItem.getOverview());
        textRelease.setText(" : " + movieItem.getRelease());
        Log.d(Const.TAG_CEK, "sumber gambar " + movieItem.getBackdrop());
        Picasso.get().load(Const.URL_IMAGES+ movieItem.getBackdrop()).into(imageDropdown);
        Picasso.get().load(Const.URL_IMAGES+movieItem.getPoster()).into(imagePoster);

       // String json = getIntent().getStringExtra(Const.MOVIE_ITEM);
        loadDataSQLite();

        if (isFavorite) {
            imageFavorite.setImageResource(R.drawable.ic_favorite_red_36dp);
        } else {
            imageFavorite.setImageResource(R.drawable.ic_favorite_border_black_36dp);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_btn_buy:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.cgv.id/en/movies/now_playing"));
                startActivity(intent);
                break;
            case R.id.detail_img_favorite:
                isFavorite = !isFavorite;
                setFavorite();
                if(isFavorite){
                    FavoriteSave();
                }else {
                    FavoriteRemove();
                }
                break;
        }
    }

    private void loadDataSQLite() {
        favoriteHelper = new FavoriteHelper(this);
        favoriteHelper.open();

        Cursor cursor = getContentResolver().query(
                Uri.parse(CONTENT_URI + "/" + movieItem.getId()),
                null,
                null,
                null,
                null
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) isFavorite = true;
            cursor.close();
        }

        setFavorite();

    }

    private void setFavorite() {
        if (isFavorite) {
            imageFavorite.setImageResource(R.drawable.ic_favorite_red_36dp);
            //Toast.makeText(this, "Favorite", Toast.LENGTH_SHORT).show();
        } else {
            imageFavorite.setImageResource(R.drawable.ic_favorite_border_black_36dp);
            //Toast.makeText(this, "Batal", Toast.LENGTH_SHORT).show();
        }
    }

    private void FavoriteSave() {
        ContentValues cv = new ContentValues();
        cv.put(MovieColumns.ID, movieItem.getId());
        cv.put(MovieColumns.TITLE, movieItem.getTitle());
        cv.put(MovieColumns.BACKDROP, movieItem.getBackdrop());
        cv.put(MovieColumns.POSTER, movieItem.getPoster());
        cv.put(MovieColumns.OVERVIEW, movieItem.getOverview());
        cv.put(MovieColumns.RELEASE, movieItem.getRelease());
        cv.put(MovieColumns.VOTE, movieItem.getVote());

        getContentResolver().insert(CONTENT_URI, cv);
        Toast.makeText(this, "you link this movie", Toast.LENGTH_SHORT).show();

    }

    private void FavoriteRemove() {
        getContentResolver().delete(
                 Uri.parse(CONTENT_URI + "/" + movieItem.getId()),
                null,
                null
        );
    Toast.makeText(this, "Sudah dihapus dari favorite list!",Toast.LENGTH_SHORT).show();
    }
}
