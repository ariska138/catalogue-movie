package com.ariskahidayat.mycatalogmovie.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.ariskahidayat.mycatalogmovie.Adapter.MoviesPagerAdapter;
import com.ariskahidayat.mycatalogmovie.Connection.Connect;
import com.ariskahidayat.mycatalogmovie.R;
import com.ariskahidayat.mycatalogmovie.Schedule.SchedulePreference;
import com.ariskahidayat.mycatalogmovie.Schedule.ScheduleReceiver;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 8:23 AM
 ******************************************************************************/

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupTabs();
    }

    private void setupTabs(){
        //setup toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        // inisialisasi tab dan pager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        //set object adapter kedalam viewpager
        viewPager.setAdapter(new MoviesPagerAdapter(getSupportFragmentManager()));

        //setup ke viewpager
        tabLayout.setupWithViewPager(viewPager);

        //konfigurasi grafity fill untuk tab berapa di posisi yang proposinal
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_search:
                Intent intent = new Intent(MainActivity.this,SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_settings:
                Intent intent2 = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(intent2);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}