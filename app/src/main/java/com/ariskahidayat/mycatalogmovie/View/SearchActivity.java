package com.ariskahidayat.mycatalogmovie.View;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.LoadData.MovieAdapter;
import com.ariskahidayat.mycatalogmovie.LoadData.SearchMovieAsyncTaskLoader;
import com.ariskahidayat.mycatalogmovie.R;

import java.util.ArrayList;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 9:33 AM
 ******************************************************************************/

public class SearchActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>{

    private static String SEARCH_TITLE = "search_title";
    ListView listView;
    MovieAdapter adapter;

    ProgressBar progressBar;

    EditText editTitle;
    Button buttonCari;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

//        getSupportActionBar().setTitle("Pencarian Movie");
       editTitle = (EditText) findViewById(R.id.search_edit_title);
      buttonCari = (Button) findViewById(R.id.search_btn_cari);
      buttonCari.setOnClickListener(mOnCari);

      progressBar = (ProgressBar) findViewById(R.id.search_progress_bar);

      adapter = new MovieAdapter(this);
      adapter.notifyDataSetChanged();
        listView = (ListView) findViewById(R.id.search_listview);
      listView.setAdapter(adapter);
      listView.setOnItemClickListener(mAdapterOnClickListener);

initLoadData();
    }

    private AdapterView.OnItemClickListener mAdapterOnClickListener = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
            // TODO Auto-generated method stub
            Movie item = (Movie) adapter.getItem(position);
            Intent movieItemIntent = new Intent(SearchActivity.this,DetailActivity.class);
            movieItemIntent.putExtra(Const.INFO_DETAIL,item);
            movieItemIntent.putExtra(Const.TITLE_DETAIL,"Detail: "+item.getTitle());
            movieItemIntent.putExtra(Const.MOVIE_READY,false);
            startActivity(movieItemIntent);
        }
    };

    private View.OnClickListener mOnCari = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
resetLoadData();
        }
    };

    private void initLoadData(){
        progressBar.setVisibility(View.VISIBLE);
        String title = editTitle.getText().toString();
        if(TextUtils.isEmpty(title)) return;
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_TITLE,title);
        getLoaderManager().initLoader(0,bundle, this);
    }

    private void resetLoadData(){
        progressBar.setVisibility(View.VISIBLE);
        String title = editTitle.getText().toString();
        if(TextUtils.isEmpty(title)) return;
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_TITLE,title);
        getLoaderManager().restartLoader(0,bundle, this);
    }

    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int id, Bundle args) {
        String title = "";
        if(args != null){
           title = args.getString(SEARCH_TITLE);
        }
        return new SearchMovieAsyncTaskLoader(this,title);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> data) {
        adapter.setData(data);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
        adapter.setData(null);
    }
}
