package com.ariskahidayat.mycatalogmovie.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.R;
import com.ariskahidayat.mycatalogmovie.Schedule.SchedulePreference;
import com.ariskahidayat.mycatalogmovie.Schedule.ScheduleReceiver;

public class SettingsActivity extends AppCompatActivity {

    ScheduleReceiver scheduleReceiver;
    SchedulePreference schedulePreference;

    ToggleButton btnDaily, btnToday;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

         btnDaily = (ToggleButton) findViewById(R.id.settings_btn_daily);
         btnToday = (ToggleButton) findViewById(R.id.settings_btn_today);

        scheduleReceiver = new ScheduleReceiver();
        schedulePreference = new SchedulePreference(getApplicationContext());

        if(schedulePreference.getDailyRemember()){
            btnDaily.setChecked(true);
        }else {
            btnDaily.setChecked(false);
        }

        if(schedulePreference.getTodayRemember()){
            btnToday.setChecked(true);
        }else {
            btnToday.setChecked(false);
        }

        btnDaily.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    scheduleReceiver.setSchedule7oclock(getBaseContext());
                    schedulePreference.setDailyRemember(true);
                    Toast.makeText(getBaseContext(),"Daily Remember diaktifkan",Toast.LENGTH_SHORT).show();
                } else {
                    // The toggle is disabled
                    scheduleReceiver.cancelAlarm(getBaseContext(), Const.EVERY_7_OCLOCK);
                    schedulePreference.setDailyRemember(false);
                    Toast.makeText(getBaseContext(),"Daily Remember dibatalkan",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnToday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    scheduleReceiver.setSchedule8oclock(getBaseContext());
                    schedulePreference.setTodayRemember(true);
                    Toast.makeText(getBaseContext(),"Today Release Remember diaktifkan",Toast.LENGTH_SHORT).show();
                } else {
                    // The toggle is disabled
                    scheduleReceiver.cancelAlarm(getBaseContext(), Const.EVERY_8_OCLOCK);
                    schedulePreference.setTodayRemember(false);
                    Toast.makeText(getBaseContext(),"Today Release Remember dibatalkan",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
