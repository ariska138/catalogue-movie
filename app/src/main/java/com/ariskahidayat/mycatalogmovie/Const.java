package com.ariskahidayat.mycatalogmovie;

public class Const {
    public   static final String LOG_OUTPUT = "widgetoutput";
    public static final String TAG_CEK = "tag_cek";
    public static final String INFO_DETAIL = "info_detail";
    public static final String TITLE_DETAIL = "title_detail";
    public static final String MOVIE_READY = "movie_ready";
    public static final String NOW_MOVIE = "now_movie";
    public static final String FAVORITE_MOVIE = "favorite_movie";
    public static final String URL_IMAGES = "https://image.tmdb.org/t/p/w500";


    public static final String EVERY_7_OCLOCK = "every7oclock";
    public static final String EVERY_8_OCLOCK = "every8oclock";
    public static final String EXTRA_SCHEDULE = "extra_schedule";

    public static final String API_KEY = "9863794c5c852b37103e025ddb9d18a9";
}
