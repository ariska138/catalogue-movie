package com.ariskahidayat.mycatalogmovie.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ariskahidayat.mycatalogmovie.R;

public class MovieViewHolder extends RecyclerView.ViewHolder{
        TextView txTitle;
        TextView txOverview;
        TextView txRelease;
        ImageView imgPoster;

    MovieViewHolder(View itemView) {
            super(itemView);
            txTitle = (TextView) itemView.findViewById(R.id.tv_item_title) ;
            txOverview = (TextView) itemView.findViewById(R.id.tv_item_overview);
            txRelease = (TextView) itemView.findViewById(R.id.tv_item_release);
            imgPoster = (ImageView) itemView.findViewById(R.id.img_item_poster);
    }
}

