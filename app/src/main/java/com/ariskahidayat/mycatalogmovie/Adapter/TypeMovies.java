package com.ariskahidayat.mycatalogmovie.Adapter;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 12:55 PM
 Last modified 6/8/18 12:55 PM
 ******************************************************************************/

public enum TypeMovies {
    POPULER,
    NOW_PLAY,
    COMING_SOON
}
