package com.ariskahidayat.mycatalogmovie.Adapter;

import android.app.Activity;
import android.app.LoaderManager;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.R;
import com.bumptech.glide.Glide;

public class ListFavoriteAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private Cursor list;
    private Activity activity;

    public ListFavoriteAdapter(Cursor items){
        replaceAll(items);
    }

    public void replaceAll(Cursor items){
        list = items;
        notifyDataSetChanged();
    }

    public ListFavoriteAdapter(Activity activity){
        this.activity = activity;
    }

    public void setListMovie(Cursor listMovie){
        list = listMovie;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movie,parent,false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        final Movie movie = getItem(position);

        holder.txTitle.setText(movie.getTitle());
        holder.txOverview.setText(movie.getOverview());
        holder.txRelease.setText(movie.getRelease());


        Glide.with(activity)
                .load(Const.URL_IMAGES+movie.getPoster())
                //.crossFade()
                .into(holder.imgPoster);
    }

    @Override
    public int getItemCount() {
        if(list == null) return 0;
        return  list.getCount();
    }

    private Movie getItem(int position){
        if(!list.moveToPosition(position)){
            throw new IllegalStateException("Position Invalid");
        }
        return  new Movie(list);
    }
}
