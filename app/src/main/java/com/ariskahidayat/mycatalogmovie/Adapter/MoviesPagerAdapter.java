package com.ariskahidayat.mycatalogmovie.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ariskahidayat.mycatalogmovie.FragmentTabs.ComingSoonFragment;
import com.ariskahidayat.mycatalogmovie.FragmentTabs.FavoriteFragment;
import com.ariskahidayat.mycatalogmovie.FragmentTabs.NowPlayingFragment;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 11:13 AM
 Last modified 6/8/18 11:13 AM
 ******************************************************************************/

public class MoviesPagerAdapter extends FragmentStatePagerAdapter {


    String[] title = new String[]{
      "Sedang Tayang","Akan Datang","Favorit"
    };

    public MoviesPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      Fragment fragment = null;
        switch (position) {
            case 0:
                 fragment = new NowPlayingFragment();
                break;
            case 1:
                fragment  = new ComingSoonFragment();
                break;
            case 2:
                fragment = new FavoriteFragment();
               break;
        }
        return  fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return title.length;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
