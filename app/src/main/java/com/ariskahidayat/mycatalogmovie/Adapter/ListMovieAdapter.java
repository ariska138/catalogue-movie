package com.ariskahidayat.mycatalogmovie.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.View.MainActivity;
import com.ariskahidayat.mycatalogmovie.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 4:20 PM
 Last modified 6/8/18 4:20 PM
 ******************************************************************************/

public class ListMovieAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private Context context;

    private ArrayList<Movie> listMovies;

    ArrayList<Movie> getListMovies() {
        return listMovies;
    }

    public Movie getItem(int index){
        return listMovies.get(index);
    }

    public void setListMovies(ArrayList<Movie> listMovies) {
        this.listMovies = listMovies;
    }

    public ListMovieAdapter(Context context) {
        this.context = context;
    }


    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movie, parent, false);
        MovieViewHolder holder = new MovieViewHolder(itemRow);
        Log.d(Const.TAG_CEK, "onCreate");
        return holder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.txTitle.setText(getListMovies().get(position).getTitle());
        holder.txOverview.setText(getListMovies().get(position).getOverview());
        holder.txRelease.setText(context.getString(R.string.release) + getListMovies().get(position).getRelease());
        Glide.with(context)
                .load(Const.URL_IMAGES+getListMovies().get(position).getPoster())
                //.crossFade()
                .into(holder.imgPoster);
    }

    @Override
    public int getItemCount() {
        if (getListMovies() == null) return 0;
        return getListMovies().size();
    }
}
