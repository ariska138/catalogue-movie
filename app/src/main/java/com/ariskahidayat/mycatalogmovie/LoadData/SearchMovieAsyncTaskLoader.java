package com.ariskahidayat.mycatalogmovie.LoadData;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.View.MainActivity;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 10:28 AM
 ******************************************************************************/

public class SearchMovieAsyncTaskLoader extends AsyncTaskLoader<ArrayList<Movie>> {

        private  ArrayList<Movie> mMovieItem;
        private static final String API_KEY = "9863794c5c852b37103e025ddb9d18a9";
        private boolean mHasResult = false;
        private String titleMovie;

        public SearchMovieAsyncTaskLoader(Context context, String titleMovie) {
            super(context);

            onContentChanged();
            Log.d(Const.TAG_CEK,"init AsycTaskLoader");
            this.titleMovie = titleMovie;
        }
        @Override
        protected void onStartLoading() {
            if (takeContentChanged())
                forceLoad();
            else if (mHasResult)
                deliverResult(mMovieItem);
        }

        @Override
        public void deliverResult(final ArrayList<Movie> data) {
            mMovieItem = data;
            mHasResult = true;
            super.deliverResult(data);
        }

        @Override
        protected void onReset() {
            super.onReset();
            onStopLoading();
            if (mHasResult) {
                mMovieItem = null;
                mHasResult = false;
            }
        }

        @Override
        public ArrayList<Movie> loadInBackground() {

            SyncHttpClient client = new SyncHttpClient();

            final ArrayList<Movie> movieItems = new ArrayList<>();

            String url = "https://api.themoviedb.org/3/search/movie?api_key="+API_KEY+"&language=en-US&query="+titleMovie;
            Log.d(Const.TAG_CEK,"get URL "+url);

            client.get(url, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String result = new String(responseBody);
                    Log.d(Const.TAG_CEK,"get URL "+result);
                    try {
                        JSONObject responseObject = new JSONObject(result);
                        JSONArray list = responseObject.getJSONArray("results");
                        for (int i=0; i < list.length() ; i++){
                            JSONObject movie = list.getJSONObject(i);
                            Movie movieItem = new Movie(movie);
                            movieItems.add(movieItem);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d(Const.TAG_CEK,"ada error dengan ket "+e.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d(Const.TAG_CEK,"Gagal Load data");
                }
            });
            return movieItems;
        }
    }

