package com.ariskahidayat.mycatalogmovie.LoadData;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;
import com.ariskahidayat.mycatalogmovie.Database.DatabaseContract;
//import  com.ariskahidayat.mycatalogmovie.Database.MovieColumns.*;

import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.BACKDROP;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.ID;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.OVERVIEW;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.POSTER;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.RELEASE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TITLE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.VOTE;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 8:23 AM
 ******************************************************************************/

public class Movie implements Parcelable {
    private int id;
    private double vote;
    private String title;
    private String poster;
    private String backdrop;
    private  String overview;
    private String release;

    public Movie(JSONObject object) throws JSONException {
        id = object.getInt("id");
        vote = object.getDouble("vote_average");
        title = object.getString("title");
        poster = object.getString("poster_path");
        backdrop = object.getString("backdrop_path");
        overview = object.getString("overview");
        release = object.getString("release_date");
    }

    public Movie(){

    }

    public Movie(Cursor cursor)  {
        id = DatabaseContract.getColumnInt (cursor,ID);
        vote = DatabaseContract.getColumnInt (cursor,VOTE);
        title = DatabaseContract.getColumnString (cursor,TITLE);
        poster = DatabaseContract.getColumnString (cursor,POSTER);
        backdrop = DatabaseContract.getColumnString (cursor,BACKDROP);
        overview = DatabaseContract.getColumnString (cursor,OVERVIEW);
        release = DatabaseContract.getColumnString (cursor,RELEASE);
    }


    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote() {
        return vote;
    }

    public void setVote(float vote) {
        this.vote = vote;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.vote);
        dest.writeString(this.title);
        dest.writeString(this.poster);
        dest.writeString(this.backdrop);
        dest.writeString(this.overview);
        dest.writeString(this.release);
    }

    protected Movie(Parcel in) {
        this.id = in.readInt();
        this.vote = in.readDouble();
        this.title = in.readString();
        this.poster = in.readString();
        this.backdrop = in.readString();
        this.overview = in.readString();
        this.release = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
