package com.ariskahidayat.mycatalogmovie.LoadData;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.View.MainActivity;
import com.ariskahidayat.mycatalogmovie.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 10:28 AM
 ******************************************************************************/

public class MovieAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<Movie> mMovieItems;

    public MovieAdapter(Context context) {
        this.context = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setData(ArrayList<Movie> items){
        mMovieItems = items;
        notifyDataSetChanged();
    }

    public  void addItem(final Movie item){
        mMovieItems.add(item);
        notifyDataSetChanged();
    }

    public void clearData(){
        mMovieItems.clear();
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    @Override
    public int getCount() {
        if(mMovieItems == null) return 0;
        return mMovieItems.size();
    }

    @Override
    public Object getItem(int position) {
       return mMovieItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Log.d(Const.TAG_CEK,"cek "+convertView);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_row_movie,null);
            holder.textViewTitle = (TextView) convertView.findViewById(R.id.tv_item_title) ;
            holder.textViewOverview = (TextView) convertView.findViewById(R.id.tv_item_overview);
            holder.textViewRelease = (TextView) convertView.findViewById(R.id.tv_item_release);
            holder.imageViewPoster = (ImageView) convertView.findViewById(R.id.img_item_poster);
            convertView.setTag(holder);
            Log.d(Const.TAG_CEK,"Init Holder");
        }else {
            holder = (ViewHolder) convertView.getTag();
            Log.d(Const.TAG_CEK,"Init Holder sudah terbuah sebelumnya");
        }
        String title = mMovieItems.get(position).getTitle();
        if(title.length() > 20){
            title = title.substring(0,20);
            title = title.concat("...");
        }
        holder.textViewTitle.setText(title);
        String overview = mMovieItems.get(position).getOverview();
        if(overview.length() > 30){
            overview = overview.substring(0,30);
            overview = overview.concat("...");
        }
        holder.textViewOverview.setText(overview);
        String release = "Release: "+mMovieItems.get(position).getRelease();
        holder.textViewRelease.setText(release);
        Picasso.get().load(mMovieItems.get(position).getPoster()).into(holder.imageViewPoster);
        return convertView;
    }

    private static class ViewHolder {
        TextView textViewTitle;
        TextView textViewOverview;
        TextView textViewRelease;
        ImageView imageViewPoster;
    }
}


