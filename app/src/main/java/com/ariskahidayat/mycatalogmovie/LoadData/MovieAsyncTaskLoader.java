package com.ariskahidayat.mycatalogmovie.LoadData;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.Adapter.TypeMovies;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 9:37 AM
 ******************************************************************************/


public class MovieAsyncTaskLoader extends AsyncTaskLoader<ArrayList<Movie>> {

    private ITaskComplated listener = null;
    private  ArrayList<Movie> mMovieItem;
    private Context context;

    private boolean mHasResult = false;

    private TypeMovies type;

    public MovieAsyncTaskLoader(Context context, TypeMovies type, ITaskComplated listener) {
        super(context);
        this.type = type;
        this.listener = listener;
        this.context = context;
        onContentChanged();
        Log.d(Const.TAG_CEK,"init AsycTaskLoader");
    }


    @Override
    protected void onStartLoading() {
        if (takeContentChanged())
            forceLoad();
        else if (mHasResult)
            deliverResult(mMovieItem);
    }

    @Override
    public void deliverResult(final ArrayList<Movie> data) {
        mMovieItem = data;
        mHasResult = true;
        super.deliverResult(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
        if (mHasResult) {
            mMovieItem = null;
            mHasResult = false;
        }
    }

    @Override
    public ArrayList<Movie> loadInBackground() {

        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<Movie> movieItems = new ArrayList<>();

        String url = "";

        switch (type){
            case POPULER:
                url = "https://api.themoviedb.org/3/movie/popular?api_key="+Const.API_KEY+"&language=en-US&page=1";
                break;
            case NOW_PLAY:
                url = "https://api.themoviedb.org/3/movie/now_playing?api_key="+Const.API_KEY+"&language=en-US&page=1";
                break;
            case COMING_SOON:
                url = "https://api.themoviedb.org/3/movie/upcoming?api_key="+Const.API_KEY+"&language=en-US&page=1";
                break;
        }

        Log.d(Const.TAG_CEK,url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i=0; i < list.length() ; i++){
                        JSONObject movie = list.getJSONObject(i);
                        Movie movieItem = new Movie(movie);
                        movieItems.add(movieItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(Const.TAG_CEK,"ada error dengan ket "+e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d(Const.TAG_CEK,"Gagal Load data");
                listener.onTaskFailure();


            }
        });

        return movieItems;
    }
}
