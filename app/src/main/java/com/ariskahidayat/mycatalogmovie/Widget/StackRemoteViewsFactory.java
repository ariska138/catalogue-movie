package com.ariskahidayat.mycatalogmovie.Widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.Database.CursorMovie;
import com.ariskahidayat.mycatalogmovie.Database.FavoriteHelper;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.ariskahidayat.mycatalogmovie.Database.DatabaseContract.CONTENT_URI;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.BACKDROP;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.ID;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.OVERVIEW;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.POSTER;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.RELEASE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TITLE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.VOTE;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory{


    private Context context;
    private int mAppWidgetID;
    private List<Movie> listMovie;
    private FavoriteHelper favoriteHelper;

    private Cursor list;

    public StackRemoteViewsFactory(Context context, Intent intent) {
        this.context = context;
        mAppWidgetID = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);

        favoriteHelper = new FavoriteHelper(context);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {

        final long identityToken = Binder.clearCallingIdentity();

        list = context.getContentResolver().query(
                CONTENT_URI,
                null,
                null,
                null,
                null
        );

        initData(context);



        Binder.restoreCallingIdentity(identityToken);

        Log.d(Const.LOG_OUTPUT,"onDataSetChanged "+ (list != null));


    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        Log.d(Const.LOG_OUTPUT,"jumlah "+listMovie.size());
      //  return list.getCount(); // pastikan jangan lupa
return listMovie.size();
   //     return 4;
    }

    @Override
    public RemoteViews getViewAt(int position) {
       Movie item = listMovie.get(position);
        Log.d(Const.LOG_OUTPUT,"title "+item.getTitle());
       RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_item);
        Log.d(Const.LOG_OUTPUT,"remoteview "+(rv != null));
        Bitmap bmp = null;
        try {
            bmp = Glide.with(context)
                    .load(Const.URL_IMAGES+item.getBackdrop())
                    .asBitmap()
                    //    .error(new ColorDrawable(context.getResources().getColor(R.color.colorPrimaryDark)))
                    .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
        }catch (InterruptedException | ExecutionException e){
            Log.d(Const.LOG_OUTPUT,"error");
        }
        rv.setImageViewBitmap(R.id.imageView,bmp);
        Log.d(Const.LOG_OUTPUT,"Image "+(bmp != null));

        Bundle extras = new Bundle();
        extras.putString(ShowMoviesWidget.EXTRA_ITEM, item.getTitle());
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);

        rv.setOnClickFillInIntent(R.id.imageView,fillInIntent);

        rv.setTextViewText(R.id.title_movie_widget,item.getTitle());
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private void initData(Context context){
        ArrayList<Movie> arrayList = new ArrayList<Movie>();
        list.moveToFirst();
        Movie movie;
        if (list.getCount()>0) {
            do {
                movie = new Movie();
                movie.setId(list.getInt(list.getColumnIndexOrThrow(ID)));
                movie.setTitle(list.getString(list.getColumnIndexOrThrow(TITLE)));
                movie.setPoster(list.getString(list.getColumnIndexOrThrow(POSTER)));
                movie.setRelease(list.getString(list.getColumnIndexOrThrow(RELEASE)));
                movie.setBackdrop(list.getString(list.getColumnIndexOrThrow(BACKDROP)));
                movie.setVote(list.getFloat(list.getColumnIndexOrThrow(VOTE)));
                movie.setOverview(list.getString(list.getColumnIndexOrThrow(OVERVIEW)));
                arrayList.add(movie);
                list.moveToNext();

            } while (!list.isAfterLast());
        }
        list.close();

        listMovie = arrayList;
    }
}
