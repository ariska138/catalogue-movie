package com.ariskahidayat.mycatalogmovie.Widget;

import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViewsService;

import com.ariskahidayat.mycatalogmovie.Const;

public class StackWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        Log.d(Const.LOG_OUTPUT,"Start Services");
        return new StackRemoteViewsFactory(this.getApplicationContext(),intent);
    }
}
