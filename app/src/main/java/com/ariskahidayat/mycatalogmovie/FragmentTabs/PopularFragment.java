package com.ariskahidayat.mycatalogmovie.FragmentTabs;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ariskahidayat.mycatalogmovie.Adapter.TypeMovies;
import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.ITaskComplated;
import com.ariskahidayat.mycatalogmovie.View.DetailActivity;
import com.ariskahidayat.mycatalogmovie.LoadData.ItemClickSupport;
import com.ariskahidayat.mycatalogmovie.Adapter.ListMovieAdapter;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.LoadData.MovieAsyncTaskLoader;
import com.ariskahidayat.mycatalogmovie.R;

import java.util.ArrayList;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 11:11 AM
 Last modified 6/8/18 11:11 AM
 ******************************************************************************/


public class PopularFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>, ITaskComplated {


    private static final String POPULAR_MOVIE = "popular_movie";

    MovieAsyncTaskLoader movieData;
    ProgressBar progressBar;
    RecyclerView recycleView;
    ArrayList<Movie> list;

    ListMovieAdapter listMovieAdapter;

    public PopularFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_popular, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.popular_progress_bar);
        recycleView = (RecyclerView) rootView.findViewById(R.id.popular_lv_movie);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity().getApplicationContext());
        recycleView.setLayoutManager(manager);
        recycleView.setHasFixedSize(true);

        listMovieAdapter = new ListMovieAdapter(getActivity().getApplicationContext());

        recycleView.setAdapter(listMovieAdapter);

        Log.d(Const.TAG_CEK, " Start Fragment Popular " + this.toString());
        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLoadData();
    }

    private void initLoadData() {
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle = new Bundle();
        bundle.putString(POPULAR_MOVIE, "init");
        getLoaderManager().initLoader(0, bundle, this);
    }

    private void resetLoadData() {
        progressBar.setVisibility(View.VISIBLE);
        Bundle bundle = new Bundle();
        bundle.putString(POPULAR_MOVIE, "reset");
        getLoaderManager().restartLoader(0, bundle, this);
    }


    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int id, Bundle args) {
        Log.d(Const.TAG_CEK, "Load data  at " + this.toString());
        return new MovieAsyncTaskLoader(getContext(), TypeMovies.POPULER, this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> data) {
        if (data.isEmpty()){
            Toast.makeText(getContext(), "Data gagal di Load, Silahkan cek koneksi Anda!", Toast.LENGTH_SHORT ).show();
        }
        Log.d(Const.TAG_CEK, "Load data selesai at " + this.toString());

        recycleView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);

        list = new ArrayList<>();
        list.addAll(data);
//        Log.d(TAG_CEK, "data "+data.size()+" at "+this.toString()+" loading "+progressBar.getVisibility());
        listMovieAdapter.setListMovies(data);

        listMovieAdapter.notifyDataSetChanged();

        ItemClickSupport.addTo(recycleView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                // TODO Auto-generated method stub
                Movie item = (Movie) list.get(position);
                Intent movieItemIntent = new Intent(getContext(),DetailActivity.class);
                movieItemIntent.putExtra(Const.INFO_DETAIL,item);
                movieItemIntent.putExtra(Const.TITLE_DETAIL,"Detail: "+item.getTitle());
                movieItemIntent.putExtra(Const.MOVIE_READY,true);
                startActivity(movieItemIntent);
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
        listMovieAdapter.setListMovies(null);
        Log.d(Const.TAG_CEK, "Reset data  at " + this.toString());
    }

    @Override
    public void onTaskFailure() {

    }
}
