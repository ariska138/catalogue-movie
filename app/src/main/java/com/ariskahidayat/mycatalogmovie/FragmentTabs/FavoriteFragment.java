package com.ariskahidayat.mycatalogmovie.FragmentTabs;


import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.ariskahidayat.mycatalogmovie.Adapter.ListFavoriteAdapter;
import com.ariskahidayat.mycatalogmovie.Adapter.ListMovieAdapter;
import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.ItemClickSupport;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.R;
import com.ariskahidayat.mycatalogmovie.View.DetailActivity;

import java.util.ArrayList;

import static com.ariskahidayat.mycatalogmovie.Database.DatabaseContract.CONTENT_URI;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.BACKDROP;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.ID;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.OVERVIEW;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.POSTER;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.RELEASE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TITLE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.VOTE;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {

    ProgressBar progressBar;
    RecyclerView recycleView;
    Cursor list;
    ListFavoriteAdapter listFavoriteAdapter;
    public FavoriteFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorite, container, false);

        progressBar = (ProgressBar) rootView.findViewById(R.id.fav_progress_bar);
        recycleView = (RecyclerView) rootView.findViewById(R.id.fav_lv_movie);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity().getApplicationContext());
        recycleView.setLayoutManager(manager);
        recycleView.setHasFixedSize(true);

        listFavoriteAdapter = new ListFavoriteAdapter(getActivity());

        recycleView.setAdapter(listFavoriteAdapter);

        new LoadFavoriteDB().execute();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        super.onResume();

        new LoadFavoriteDB().execute();
    }

    private class LoadFavoriteDB extends AsyncTask<Void,Void,Cursor>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }


        @Override
        protected Cursor doInBackground(Void... voids) {
            return getContext().getContentResolver().query(
                    CONTENT_URI,
                    null,
                    null,
                    null,
                    null
            );
        }


        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);

            list = cursor;
            listFavoriteAdapter.replaceAll(cursor);
            recycleView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.INVISIBLE);
            listFavoriteAdapter.notifyDataSetChanged();

            ItemClickSupport.addTo(recycleView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    // TODO Auto-generated method stub
                    list.moveToPosition(position);

                           Movie movie = new Movie();
                            movie.setId(list.getInt(list.getColumnIndexOrThrow(ID)));
                            movie.setTitle(list.getString(list.getColumnIndexOrThrow(TITLE)));
                            movie.setPoster(list.getString(list.getColumnIndexOrThrow(POSTER)));
                            movie.setRelease(list.getString(list.getColumnIndexOrThrow(RELEASE)));
                            movie.setBackdrop(list.getString(list.getColumnIndexOrThrow(BACKDROP)));
                            movie.setVote(list.getFloat(list.getColumnIndexOrThrow(VOTE)));
                            movie.setOverview(list.getString(list.getColumnIndexOrThrow(OVERVIEW)));

                    list.close();
                    Intent movieItemIntent = new Intent(getContext(),DetailActivity.class);
                    movieItemIntent.putExtra(Const.INFO_DETAIL,movie);
                    movieItemIntent.putExtra(Const.TITLE_DETAIL,"Detail: "+movie.getTitle());
                    movieItemIntent.putExtra(Const.MOVIE_READY,true);
                    startActivity(movieItemIntent);
                }
            });
        }
    }

}
