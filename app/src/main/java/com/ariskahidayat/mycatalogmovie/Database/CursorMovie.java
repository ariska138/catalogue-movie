package com.ariskahidayat.mycatalogmovie.Database;

import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import static android.provider.BaseColumns._ID;
import static com.ariskahidayat.mycatalogmovie.Database.DatabaseContract.getColumnString;
import static com.ariskahidayat.mycatalogmovie.Database.DatabaseContract.getColumnInt;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.BACKDROP;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.OVERVIEW;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.RELEASE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.POSTER;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TITLE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.ID;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.VOTE;

public class CursorMovie {
    @SerializedName("id")
    private int id;
    @SerializedName("vote")
    private double vote;
    @SerializedName("title")
    private String title;
    @SerializedName("poster")
    private String poster;
    @SerializedName("backdrop")
    private String backdrop;
    @SerializedName("overview")
    private  String overview;
    @SerializedName("release")
    private String release;

    public CursorMovie() {

    }


    public CursorMovie(Cursor cursor) {
        this.id = getColumnInt(cursor,ID);
        this.title = getColumnString(cursor,TITLE);
        this.poster = getColumnString(cursor,POSTER);
        this.release = getColumnString(cursor, RELEASE);
        this.backdrop = getColumnString(cursor,BACKDROP);
        this.overview = getColumnString(cursor, OVERVIEW);
        this.vote = getColumnInt(cursor, VOTE);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote() {
        return vote;
    }

    public void setVote(double vote) {
        this.vote = vote;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    @Override
    public String toString(){
        return "CursorMovie{"+
                "id = '"+id+'\''+
                "title = '"+title+'\''+
                "release = '"+release+'\''+
                "backdrop = '"+backdrop+'\''+
                "overview = '"+overview+'\''+
                "vote = '"+vote+'\''+
                "poster = '"+poster+'\''+
                "}";
    }
}
