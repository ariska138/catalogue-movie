package com.ariskahidayat.mycatalogmovie.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.ariskahidayat.mycatalogmovie.LoadData.Movie;

import java.util.ArrayList;

import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.ID;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.BACKDROP;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.OVERVIEW;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.RELEASE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.POSTER;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TABLE_MOVIE;
import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TITLE;

import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.VOTE;

public class FavoriteHelper {
    private static String DATABASE_TABLE = TABLE_MOVIE;
    private Context context;
    private DatabaseHelper dataBaseHelper;

    private SQLiteDatabase database;

    public FavoriteHelper(Context context){
        this.context = context;
    }

    public FavoriteHelper open() throws SQLException {
        dataBaseHelper = new DatabaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dataBaseHelper.close();
    }

    public ArrayList<Movie> query() {
        ArrayList<Movie> arrayList = new ArrayList<Movie>();
        Cursor cursor = database.query(DATABASE_TABLE,null,null,null,null,null,ID +" DESC",null);
        cursor.moveToFirst();
        Movie movie;
        if (cursor.getCount()>0) {
            do {
                movie = new Movie();
                movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(ID)));
                movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                movie.setPoster(cursor.getString(cursor.getColumnIndexOrThrow(POSTER)));
                movie.setRelease(cursor.getString(cursor.getColumnIndexOrThrow(RELEASE)));
                movie.setBackdrop(cursor.getString(cursor.getColumnIndexOrThrow(BACKDROP)));
                movie.setVote(cursor.getFloat(cursor.getColumnIndexOrThrow(VOTE)));
                movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(OVERVIEW)));
                arrayList.add(movie);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    public long insert(Movie movie){
        ContentValues initialValues =  new ContentValues();
        initialValues.put(TITLE, movie.getTitle());
        initialValues.put(POSTER, movie.getPoster());
        initialValues.put(RELEASE, movie.getRelease());
        initialValues.put(ID,movie.getId());
        initialValues.put(OVERVIEW,movie.getOverview());
        initialValues.put(VOTE,movie.getVote());
        initialValues.put(BACKDROP,movie.getBackdrop());
        return database.insert(DATABASE_TABLE, null, initialValues);
    }

    public int update(Movie movie){
        ContentValues args = new ContentValues();
        args.put(TITLE, movie.getTitle());
        args.put(POSTER, movie.getPoster());
        args.put(RELEASE, movie.getRelease());
        args.put(ID,movie.getId());
        args.put(OVERVIEW,movie.getOverview());
        args.put(VOTE,movie.getVote());
        args.put(BACKDROP,movie.getBackdrop());
        return database.update(DATABASE_TABLE, args, ID + "= '" + movie.getId() + "'", null);
    }

    public int delete(int id){
        return database.delete(TABLE_MOVIE, ID + " = '"+id+"'", null);
    }

    /*
    Content Provider
     */

    public Cursor queryByIdProvider(String id){
        return database.query(DATABASE_TABLE,null
                ,ID + " = ?"
                ,new String[]{id}
                ,null
                ,null
                ,null
                ,null);
    }
    public Cursor queryProvider(){
        return database.query(DATABASE_TABLE
                ,null
                ,null
                ,null
                ,null
                ,null
                ,ID + " DESC");
    }
    public long insertProvider(ContentValues values){
        return database.insert(DATABASE_TABLE,null,values);
    }
    public int updateProvider(String id,ContentValues values){
        return database.update(DATABASE_TABLE,values,ID +" = ?",new String[]{id} );
    }
    public int deleteProvider(String id){
        return database.delete(DATABASE_TABLE,ID + " = ?", new String[]{id});
    }
}
