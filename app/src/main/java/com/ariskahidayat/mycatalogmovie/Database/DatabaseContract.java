package com.ariskahidayat.mycatalogmovie.Database;

import android.database.Cursor;
import android.net.Uri;

import static com.ariskahidayat.mycatalogmovie.Database.MovieColumns.TABLE_MOVIE;

public class DatabaseContract {



    /*
    yang digunakan untuk content provider
     */

    public static final String AUTHORITY = "com.ariskahidayat.mycatalogmovie";
    public static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(AUTHORITY)
            .appendPath(TABLE_MOVIE)
            .build();
    public static String getColumnString(Cursor cursor, String columnName) {
        return cursor.getString( cursor.getColumnIndex(columnName) );
    }
    public static int getColumnInt(Cursor cursor, String columnName) {
        return cursor.getInt( cursor.getColumnIndex(columnName) );
    }
    public static long getColumnLong(Cursor cursor, String columnName) {
        return cursor.getLong( cursor.getColumnIndex(columnName) );
    }
}
