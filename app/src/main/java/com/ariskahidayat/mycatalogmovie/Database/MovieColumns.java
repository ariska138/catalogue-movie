package com.ariskahidayat.mycatalogmovie.Database;

import android.provider.BaseColumns;

    public final class MovieColumns implements BaseColumns { // ini bisa di class sendiri

        public static String TABLE_MOVIE = "movie";

        //Id
        public static String ID = "id";
        //Note title
        public static String TITLE = "title"; // seharuskan diawali dengan kata COLUMN
        //Note description
        public static String POSTER = "poster";
        //Note date
        public static String RELEASE = "release";
        //
        public static String VOTE = "vote";

        public static String BACKDROP = "backdrop";

        public static String OVERVIEW = "overview";

}
