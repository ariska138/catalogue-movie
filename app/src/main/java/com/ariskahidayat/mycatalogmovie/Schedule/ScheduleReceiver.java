package com.ariskahidayat.mycatalogmovie.Schedule;

import android.app.AlarmManager;
import android.app.LoaderManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Debug;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.View.MainActivity;
import com.ariskahidayat.mycatalogmovie.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ScheduleReceiver extends BroadcastReceiver{


    private final int NOTIF_ID_7OCLOCK = 100;
    private final int NOTIF_ID_8OCLOCK = 101;

    private  NotificationUtils mNotificationUtils;

    @Override
    public void onReceive(final Context context, Intent intent) {
    final String schedule = intent.getStringExtra(Const.EXTRA_SCHEDULE);

    //tindakan khusus untuk jam 8
        if(schedule.equalsIgnoreCase(Const.EVERY_8_OCLOCK) ){
            // melakukan pencarian apakah tanggal sekarang sama dengan tanggal release
            final BroadcastReceiver.PendingResult result=goAsync();

            (new Thread() {
                public void run() {
                    SyncHttpClient client = new SyncHttpClient();

                    final ArrayList<Movie> movieItems = new ArrayList<>();

                    String url = "";
                    url = "https://api.themoviedb.org/3/movie/now_playing?api_key="+Const.API_KEY+"&language=en-US&page=1";

                    client.get(url, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            String result = new String(responseBody);
                            String title = "Test";
                            String message = "Test";
                            try {
                                JSONObject responseObject = new JSONObject(result);
                                JSONArray list = responseObject.getJSONArray("results");

                                for (int i=0; i < list.length() ; i++){
                                    JSONObject movie = list.getJSONObject(i);
                                    Movie movieItem = new Movie(movie);
                                    movieItems.add(movieItem);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d(Const.TAG_CEK,"ada error dengan ket "+e.getMessage());
                            }

                            for (int n=0; n<movieItems.size();n++){
                                String date = movieItems.get(n).getRelease();
                                //Log.d("ShowNotif",date);
                                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    Date dateTayang = formatDate.parse(date);
                                    if(Calendar.getInstance().getTime().compareTo(dateTayang) == 1){
                                        title = movieItems.get(n).getTitle();
                                        message = "Hari ini "+movieItems.get(n).getTitle()+" release";
                                        showAlarmNotification(context,title ,message , NOTIF_ID_8OCLOCK);
                                        break;
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.d(Const.TAG_CEK,"Gagal Load data");
                        }
                    });
                    result.finish();


                }
            }).start();
        }else {
            final String message = "Yuuk, Nonton!";
            final String title = "Cek movie-movie terbaru hari ini";
            showAlarmNotification(context, title, message, NOTIF_ID_7OCLOCK);
        }
    }

    private void showAlarmNotification(Context context, String title, String message, int notifId){
// do something for phones running an SDK before oreo
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Do something for lollipop and above versions
            Intent notificationIntent = new Intent(context, MainActivity.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);

            mNotificationUtils = new NotificationUtils(context);
            Notification.Builder nb = mNotificationUtils.getChannelNotification(title, message, pendingIntent);
            mNotificationUtils.getManager().notify(notifId, nb.build());
        } else {
            Intent notificationIntent = new Intent(context, MainActivity.class);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_movie)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(pendingIntent)
                    .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSound(alarmSound);

            notificationManager.notify(notifId, builder.build());
        }
    }

    public void setSchedule7oclock(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, ScheduleReceiver.class);
        intent.putExtra(Const.EXTRA_SCHEDULE, Const.EVERY_7_OCLOCK);


            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.HOUR_OF_DAY, 7);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0); // digenapkan ke menit

            calendar.add(Calendar.DAY_OF_MONTH,1);

            Log.e("Calendar", "setAlarmManager: " + calendar.getTime().toString());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, NOTIF_ID_7OCLOCK, intent, 0);

            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public void setSchedule8oclock(Context context){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, ScheduleReceiver.class);
        intent.putExtra(Const.EXTRA_SCHEDULE, Const.EVERY_8_OCLOCK);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0); // digenapkan ke menit

        calendar.add(Calendar.DAY_OF_MONTH,1);

            Log.e("Calendar", "setAlarmManager: " + calendar.getTime().toString());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, NOTIF_ID_8OCLOCK, intent, 0);

            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public void cancelAlarm(Context context, String type){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManager.class);
        int requestCode = type.equalsIgnoreCase(Const.EXTRA_SCHEDULE) ? NOTIF_ID_7OCLOCK : NOTIF_ID_8OCLOCK;
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0);
        alarmManager.cancel(pendingIntent);
    }
}
