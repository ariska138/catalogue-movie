package com.ariskahidayat.mycatalogmovie.Schedule;

import android.content.Context;
import android.content.SharedPreferences;

import com.ariskahidayat.mycatalogmovie.Const;

public class SchedulePreference {

    public final String PREF_NAME = "SchedulePreference";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor editor;

    public SchedulePreference(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setDailyRemember( Boolean value){
        saveData(Const.EVERY_7_OCLOCK,value);
    }

    public boolean getDailyRemember(){
        return loadData(Const.EVERY_7_OCLOCK);
    }

    public void setTodayRemember( Boolean value){
        saveData(Const.EVERY_8_OCLOCK,value);
    }

    public boolean getTodayRemember(){
        return  loadData(Const.EVERY_8_OCLOCK);
    }

    private void saveData(String key, Boolean data){
        editor = mSharedPreferences.edit();
        editor.putBoolean(key,data);
        editor.commit();
    }

    private Boolean loadData(String key){
        return mSharedPreferences.getBoolean(key,false);
    }

    public void cleanData(){
        editor = mSharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
