package com.ariskahidayat.mycatalogmovie.Schedule;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ariskahidayat.mycatalogmovie.Const;
import com.ariskahidayat.mycatalogmovie.LoadData.Movie;
import com.ariskahidayat.mycatalogmovie.LoadData.MovieAsyncTaskLoader;
import com.ariskahidayat.mycatalogmovie.View.MainActivity;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SetSchedule extends AsyncTask<String, String, String> {
    MovieAsyncTaskLoader movieData;
    private  ArrayList<Movie> mMovieItem;
    private static final String API_KEY = "9863794c5c852b37103e025ddb9d18a9";
    private ScheduleReceiver scheduleReceiver;
    private Context context;

    public SetSchedule(Context context) {
      this.context = context;
      scheduleReceiver = new ScheduleReceiver();
    }

    @Override
    protected String doInBackground(String... strings) {
        SyncHttpClient client = new SyncHttpClient();

        final ArrayList<Movie> movieItems = new ArrayList<>();

        String  url = "https://api.themoviedb.org/3/movie/now_playing?api_key="+API_KEY+"&language=en-US&page=1";

        Log.d("outpuut",url);
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String result = new String(responseBody);
                try {
                    JSONObject responseObject = new JSONObject(result);
                    JSONArray list = responseObject.getJSONArray("results");

                    for (int i=0; i < list.length() ; i++){
                        JSONObject movie = list.getJSONObject(i);
                        Movie movieItem = new Movie(movie);
                        movieItems.add(movieItem);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(Const.TAG_CEK,"ada error dengan ket "+e.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

        });

        return "";
    }

    protected void onPostExecute(String ab){

    }

}
