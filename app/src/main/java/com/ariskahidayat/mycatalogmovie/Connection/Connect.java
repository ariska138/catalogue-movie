package com.ariskahidayat.mycatalogmovie.Connection;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import com.ariskahidayat.mycatalogmovie.R;

/*******************************************************************************
 Created by Ariska Hidayat on 6/8/18 10:28 AM
 Last modified 6/8/18 8:23 AM
 ******************************************************************************/

public class Connect {

    ConnectivityManager ConnectionManager;

    public Connect(ConnectivityManager systemService) {
        ConnectionManager =  systemService;
    }

    public boolean checkInternet(){
        boolean connectStatus = true;

        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()==true ) {
            connectStatus = true;
        }
        else {
            connectStatus = false;
        }
        return connectStatus;
    }


}
